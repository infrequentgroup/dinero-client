import DineroRequest from './../../libs/DineroRequest.js';

export default {
  create(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/vouchers/manuel',
    );
    return req.executePost(data);
  },
  get(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/vouchers/manuel/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  update(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/vouchers/manuel/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePut(data);
  },
  delete(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/vouchers/manuel/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeDelete();
  },
  book(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/vouchers/manuel/{guid}/book',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
};
