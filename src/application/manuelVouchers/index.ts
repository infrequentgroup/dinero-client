import Methods from './methods.js';

/**
 * ManuelVouchers manager
 * @class
 */
export default class ManuelVouchers {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Creates a manuel voucher draft
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  create(data) {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Gets details of a specific manuel voucher
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  get(guid) {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Updates a manuel voucher
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data, guid) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Deletes a manuel voucher
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  delete(guid) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Books a manuel voucher
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  book(data, guid) {
    return new Promise((res, rej) => {
      Methods.book(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  processError(err) {
    console.log(err);
  }
}
