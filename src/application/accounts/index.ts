import {
  ICreateDepositAccount,
  ICreateEntryAccount,
  IDepositAccount,
  IPurchaseAccount,
} from './interface.js';
import Methods from './methods.js';

/**
 * Accounts manager
 * @class
 */
export default class Accounts {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Get the list of entry accounts for the organization
   * @returns {Promise<Object>}
   */
  listEntry(): Promise<IPurchaseAccount[]> {
    return new Promise((res, rej) => {
      Methods.listEntry(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Create a new entry account
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  createEntry(data: ICreateEntryAccount) {
    return new Promise((res, rej) => {
      Methods.createEntry(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get the list of deposit accounts for the organization
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  listDeposit(): Promise<IDepositAccount[]> {
    return new Promise((res, rej) => {
      Methods.listDeposit(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Create a new deposit account
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  createDeposit(data: ICreateDepositAccount) {
    return new Promise((res, rej) => {
      Methods.createDeposit(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err: any) {
    console.log(err);
  }
}
