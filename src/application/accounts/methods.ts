import DineroRequest from './../../libs/DineroRequest.js';

export default {
  listEntry(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/accounts/entry',
    );
    return req.executeGet();
  },
  createEntry(token: string, orgID: string, data) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/accounts/entry',
    );
    return req.executePost(data);
  },
  listDeposit(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/accounts/deposit',
    );
    return req.executeGet();
  },
  createDeposit(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/accounts/deposit',
    );
    return req.executePost(data);
  },
};
