export interface IPurchaseAccount {
  AccountNumber: number;
  Name: string;
  VatCode: string;
  Category: string;
  CategoryName: string;
  IsHidden: boolean;
  IsDefaultSalesAccount: boolean;
}
export interface ICreateEntryAccount {
  Number: number;
  Name: string;
  VatCode: string;
}
export interface IDepositAccount {
  AccountNumber: number;
  Name: string;
  IsDefault: boolean;
  IsHidden: boolean;
}

export interface ICreateDepositAccount {
  Number: number;
  Name: string;
  RegistrationNumber?: string;
  AccountNumber?: string;
  SwiftNumber?: string;
  IbanNumber: string;
}
