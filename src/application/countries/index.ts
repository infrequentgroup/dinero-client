import { ICountry } from './interface.js';
import Methods from './methods.js';

/**
 * Countries manager
 * @class
 */
export default class Countries {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Get a list of countries
   * @returns {Promise<Object>}
   */
  list(): Promise<ICountry[]> {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
