export interface ICountry {
  Key: string;
  DanishName: string;
  EnglishName: string;
}
