import DineroRequest from './../../libs/DineroRequest.js';

export default {
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/countries',
    );
    return req.executeGet();
  },
};
