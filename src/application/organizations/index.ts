import Methods from './methods.js';

/**
 * Organizations manager
 * @class
 */
export default class Organizations {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Gets a list of the users' organizations. Remember that an api key only gives access to a single organization
   * @returns {Promise<Object>}
   */
  list() {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
