import DineroFile from './../../libs/DineroFile.js';

export default {
  upload(token: string, orgID: string, file: any) {
    let req = new DineroFile(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/files',
    );
    return req.executeUpload(file);
  },
};
