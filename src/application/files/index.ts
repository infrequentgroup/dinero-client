import { IFileUpload } from './interface.js';
import Methods from './methods.js';

/**
 * Files manager
 * @class
 */
export default class Files {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Accepts a posted image. If the file is accepted status code 200 will be returned with info on the file's id and the id of the organization owning file. It might take a second for the file to be accessible.
   * @param {Object} file
   */
  upload(file): Promise<IFileUpload> {
    return new Promise((res, rej) => {
      Methods.upload(this.token, this.orgID, file)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
