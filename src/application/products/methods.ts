import DineroRequest from './../../libs/DineroRequest.js';

export default {
  get(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/products/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  update(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/products/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePut(data);
  },
  delete(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/products/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeDelete();
  },
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/products',
    );
    return req.executeGet();
  },
  create(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/products',
    );
    return req.executePost(data);
  },
};
