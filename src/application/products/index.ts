import Methods from './methods.js';

/**
 * Products manager
 * @class
 */
export default class Products {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Gets Product Information for the product with the given Id
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  get(guid) {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update an existing product
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data, guid) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete a product
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  delete(guid) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Retrieve a list of products for the organization order by UpdatedAt.
   * @returns {Promise<Object>}
   */
  list() {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Add a new product to the organization
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  create(data) {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
