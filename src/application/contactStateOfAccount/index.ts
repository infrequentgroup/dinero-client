import {
  IContactStateOfAccountEntries,
  IMailContactStateOfAccount,
} from './interface.js';
import Methods from './methods.js';

/**
 * ContactStateOfAccount manager
 * @class
 */
export default class ContactStateOfAccount {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Returns the income, expenses and related entries for a contact in the given period (or all time if no period is defined).
   */
  get(guid: string): Promise<IContactStateOfAccountEntries> {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Downloads pdf of the income, expenses and related entries for a contact in the given period (or all time if no period is defined).
   */
  getStatePdf(guid: string) {
    return new Promise((res, rej) => {
      Methods.getStatePdf(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Mails the income, expenses and related entries for a contact in the given period (or all time if no period is defined).
   */
  mailState(guid: string, data: IMailContactStateOfAccount) {
    return new Promise((res, rej) => {
      Methods.mailState(this.token, this.orgID, guid, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err: any) {
    console.log(err);
  }
}
