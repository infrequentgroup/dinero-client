export interface IContactStateOfAccountEntry {
  RelatedVoucherGuid: string;
  RelatedVoucherNumber: string;
  Date: string;
  DecayDate: string;
  Description: string;
  Type: string;
  Amount: number;
  Balance: number;
  Status: string;
  RelatedVoucherCategory: string;
  RelatedVoucherClass: string;
}

export interface IContactStateOfAccountEntries {
  ContactGuid: string;
  Income: number;
  Expenses: number;
  Entries: IContactStateOfAccountEntry[];
}
export interface IMailContactStateOfAccount {
  Sender?: string;
  CCToSender: boolean;
  Receiver?: string;
  Subject?: string;
  Message?: string;
  From?: string;
  To?: string;
  HideClosed: boolean;
}
