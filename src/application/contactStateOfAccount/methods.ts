import DineroRequest from './../../libs/DineroRequest.js';
import { IMailContactStateOfAccount } from './interface.js';

export default {
  get(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/state-of-account/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  getStatePdf(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/state-of-account/{guid}/pdf',
    );
    req.replaceField('{guid}', guid);
    return req.executeFileGet();
  },
  mailState(
    token: string,
    orgID: string,
    guid: string,
    data: IMailContactStateOfAccount,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/state-of-account/{guid}/email',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
};
