import { IPagination } from './../interface';
export interface IContactNote {
  Text: string;
  Id: string;
  NoteDate: Date;
  AuthorName: string;
  AuthorEmail: string;
  CreatedAt: Date;
  UpdatedAt: Date;
  DeletedAt: Date;
}
export interface IListContactNotes {
  Collection: IContactNote[];
  Pagination: IPagination;
}
export interface ICreateContactNote {
  Text: string;
}

export interface IUpdateContactNote {
  Text: string;
}
