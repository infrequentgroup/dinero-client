import {
  IContactNote,
  ICreateContactNote,
  IListContactNotes,
  IUpdateContactNote,
} from './interface.js';
import Methods from './methods.js';

/**
 * ContactNotes manager
 * @class
 */
export default class ContactNotes {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }

  /**
   * Retrieve a list of notes for the contact ordered by NoteDate.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  list(contactGuid: string): Promise<IListContactNotes> {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID, contactGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Add a new note for the contact
   * @param {Object} data
   */
  create(contactGuid: string, data: any): Promise<ICreateContactNote> {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, contactGuid, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Retrieve a specific note for a contact
   * @param {Object} data
   */
  get(contactGuid: string, noteGuid: string): Promise<IContactNote> {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, contactGuid, noteGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update an existing note
   * @param {Object} data
   * @param {String} contactGuid
   * @param {String} noteGuid
   */
  update(data: IUpdateContactNote, contactGuid: string, noteGuid: string) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, contactGuid, noteGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete a note from the contact
   * @param {String} contactGuid
   * @param {String} noteGuid
   */
  delete(contactGuid: string, noteGuid: string) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, contactGuid, noteGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err: any) {
    console.log(err);
  }
}
