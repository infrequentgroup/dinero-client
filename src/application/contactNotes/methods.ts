import DineroRequest from './../../libs/DineroRequest.js';
export default {
  list(token: string, orgID: string, contactGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{contactGuid}/notes',
    );
    req.replaceField('{contactGuid}', contactGuid);
    return req.executeGet();
  },
  create(token: string, orgID: string, contactGuid: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{contactGuid}/notes',
    );
    req.replaceField('{contactGuid}', contactGuid);
    return req.executePost(data);
  },
  get(token: string, orgID: string, contactGuid: string, noteGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{contactGuid}/notes/{noteGuid}',
    );
    req.replaceField('{contactGuid}', contactGuid);
    req.replaceField('{noteGuid}', noteGuid);
    return req.executeGet();
  },
  update(
    token: string,
    orgID: string,
    data: any,
    contactGuid: string,
    noteGuid: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{contactGuid}/notes/{noteGuid}',
    );
    req.replaceField('{contactGuid}', contactGuid);
    req.replaceField('{noteGuid}', noteGuid);
    return req.executePut(data);
  },
  delete(token: string, orgID: string, contactGuid: string, noteGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{contactGuid}/notes/{noteGuid}',
    );
    req.replaceField('{contactGuid}', contactGuid);
    req.replaceField('{noteGuid}', noteGuid);
    return req.executeDelete();
  },
};
