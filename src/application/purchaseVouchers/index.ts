import Methods from './methods.js';

/**
 * PurchaseVouchers manager
 * @class
 */
export default class PurchaseVouchers {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Creates a new purchase voucher draft
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  create(data) {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Gets a purchase voucher by its guid
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  get(guid) {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update a purchase voucher draft, you cannot update booked purchase vouchers
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data, guid) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete a purchase voucher
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  delete(data, guid) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Book a purchase voucher
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  book(data, guid) {
    return new Promise((res, rej) => {
      Methods.book(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Generate and saves a credit note draft of a given booked purchase voucher.
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  createCreditNote(data, guid) {
    return new Promise((res, rej) => {
      Methods.createCreditNote(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
