export interface IPagination {
  MaxPageSizeAllowed: 1000;
  PageSize: 100;
  Result: 100;
  ResultWithoutFilter: 231;
  Page: 0;
}
