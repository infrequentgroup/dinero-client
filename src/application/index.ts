import AccountingYears from './accountingYears/index.js';
import Accounts from './accounts/index.js';
import ContactNotes from './contactNotes/index.js';
import Contacts from './contacts/index.js';
import ContactStateOfAccount from './contactStateOfAccount/index.js';
import Countries from './countries/index.js';
import Entry from './entry/index.js';
import Files from './files/index.js';
import Invoices from './invoices/index.js';
import LedgerItems from './ledgerItems/index.js';
import ManuelVouchers from './manuelVouchers/index.js';
import Organizations from './organizations/index.js';
import Products from './products/index.js';
import PurchaseCreditNote from './purchaseCreditNote/index.js';
import PurchaseVouchers from './purchaseVouchers/index.js';
import Reminders from './reminders/index.js';
import Sales from './sales/index.js';
import SalesCreditNote from './salesCreditNote/index.js';
import TradeOffers from './tradeOffers/index.js';
import VatTypes from './vatTypes/index.js';

/**
 * Application class that contains all managers
 * @class
 */
export default class Application {
  token: string;
  orgId: string;
  constructor(token: string, orgId: string) {
    this.token = token;
    this.orgId = orgId;
  }
  /**
   * Get AccountingYears Manager
   * @returns {AccountingYears()}
   */
  get accountingYears(): AccountingYears {
    return new AccountingYears(this.token, this.orgId);
  }

  /**
   * Get Accounts Manager
   * @returns {Accounts()}
   */
  get accounts(): Accounts {
    return new Accounts(this.token, this.orgId);
  }

  /**
   * Get ContactNotes Manager
   * @returns {ContactNotes()}
   */
  get contactNotes(): ContactNotes {
    return new ContactNotes(this.token, this.orgId);
  }

  /**
   * Get Contacts Manager
   * @returns {Contacts()}
   */
  get contacts(): Contacts {
    return new Contacts(this.token, this.orgId);
  }

  /**
   * Get Countries Manager
   * @returns {ContactStateOfAccount()}
   */
  get contactStateOfAccount(): ContactStateOfAccount {
    return new ContactStateOfAccount(this.token, this.orgId);
  }
  /**
   * Get Countries Manager
   * @returns {Countries()}
   */
  get countries(): Countries {
    return new Countries(this.token, this.orgId);
  }
  /**
   * Get Entry Manager
   * @returns {Entry()}
   */
  get entry(): Entry {
    return new Entry(this.token, this.orgId);
  }

  /**
   * Get Files Manager
   * @returns {Files()}
   */
  get files(): Files {
    return new Files(this.token, this.orgId);
  }

  /**
   * Get Invoices Manager
   * @returns {Invoices()}
   */
  get invoices(): Invoices {
    return new Invoices(this.token, this.orgId);
  }

  /**
   * Get LedgerItems Manager
   * @returns {LedgerItems()}
   */
  get ledgerItems(): LedgerItems {
    return new LedgerItems(this.token, this.orgId);
  }

  /**
   * Get ManuelVouchers Manager
   * @returns {ManuelVouchers()}
   */
  get manuelVouchers(): ManuelVouchers {
    return new ManuelVouchers(this.token, this.orgId);
  }

  /**
   * Get Organizations Manager
   * @returns {Organizations()}
   */
  get organizations(): Organizations {
    return new Organizations(this.token, this.orgId);
  }

  /**
   * Get Products Manager
   * @returns {Products()}
   */
  get products(): Products {
    return new Products(this.token, this.orgId);
  }

  /**
   * Get PurchaseCreditNote Manager
   * @returns {PurchaseCreditNote()}
   */
  get purchaseCreditNote(): PurchaseCreditNote {
    return new PurchaseCreditNote(this.token, this.orgId);
  }

  /**
   * Get PurchaseVouchers Manager
   * @returns {PurchaseVouchers()}
   */
  get purchaseVouchers(): PurchaseVouchers {
    return new PurchaseVouchers(this.token, this.orgId);
  }

  /**
   * Get Reminders Manager
   * @returns {Reminders()}
   */
  get reminders(): Reminders {
    return new Reminders(this.token, this.orgId);
  }

  /**
   * Get Sales Manager
   * @returns {Sales()}
   */
  get sales(): Sales {
    return new Sales(this.token, this.orgId);
  }

  /**
   * Get SalesCreditNote Manager
   * @returns {SalesCreditNote()}
   */
  get salesCreditNotes(): SalesCreditNote {
    return new SalesCreditNote(this.token, this.orgId);
  }

  /**
   * Get TradeOffers Manager
   * @returns {TradeOffers()}
   */
  get tradeOffers(): TradeOffers {
    return new TradeOffers(this.token, this.orgId);
  }

  /**
   * Get VatTypes Manager
   * @returns {VatTypes()}
   */
  get vatTypes(): VatTypes {
    return new VatTypes(this.token, this.orgId);
  }
}
