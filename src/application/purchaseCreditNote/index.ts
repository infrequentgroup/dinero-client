import Methods from './methods.js';

/**
 * PurchaseCreditNote manager
 * @class
 */
export default class PurchaseCreditNote {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Book purchase credit note
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  book(data, guid) {
    return new Promise((res, rej) => {
      Methods.book(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
