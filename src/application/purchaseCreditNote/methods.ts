import DineroRequest from './../../libs/DineroRequest.js';

export default {
  book(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/vouchers/purchase/creditnotes/{guid}/book',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
};
