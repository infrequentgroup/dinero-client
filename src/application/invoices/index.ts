import {
  IBookInvoice,
  ICreateInvoice,
  IInvoiceUpdateResponse,
  IDeleteInvoice,
  IInvoiceEmailTemplate,
  IInvoiceTotalResponse,
  IListInvoices,
  IPreReminderTemplate,
  ISendInvoiceEAN,
  ISendInvoiceEmail,
  ISendInvoiceEmailResponse,
  ISendPreReminderTemplate,
  IAddPaymentToInvoice,
  IPaymentsForInvoice,
  ICreateCreditNote,
  IInvoiceTemplate,
  IMailouts,
} from './interface.js';
import Methods from './methods.js';

/**
 * Invoices manager
 * @class
 */
export default class Invoices {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Saves invoice.
   * @param {Object} data
   */
  create(data: ICreateInvoice): Promise<IInvoiceUpdateResponse> {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Retrieve a list of invoices for the organization.
   * @param {Object} data
   */
  list(): Promise<IListInvoices> {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Fetch a invoice to get total, line sums and payment date calculations.
   * @param {Object} data
   */
  getInvoiceTotal(data: ICreateInvoice): Promise<IInvoiceTotalResponse> {
    return new Promise((res, rej) => {
      Methods.getInvoiceTotal(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get invoice as pdf.
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  getInvoicePdf(guid: string) {
    return new Promise((res, rej) => {
      Methods.getInvoicePdf(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get invoice as json.
   * @param {String} guid
   */
  getInvoiceJson(guid: string): Promise<IInvoiceTotalResponse> {
    return new Promise((res, rej) => {
      Methods.getInvoiceJson(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete invoice. The invoice cannot be deleted if booked.
   * @param {Object} data
   * @param {String} guid
   */
  delete(data: IDeleteInvoice, guid: string) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Book invoice.
   * @param {Object} data
   * @param {String} guid
   */
  bookInvoice(
    data: IBookInvoice,
    guid: string,
  ): Promise<IInvoiceUpdateResponse> {
    return new Promise((res, rej) => {
      Methods.bookInvoice(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get the email template for the email with link to a public version of the invoice where it can be printed or downloaded as pdf.
   * @param {String} guid
   */
  getInvoiceEmailTemplate(guid: string): Promise<IInvoiceEmailTemplate> {
    return new Promise((res, rej) => {
      Methods.getInvoiceEmailTemplate(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an email with link to a public version of the invoice where it can be printed or downloaded as a pdf.
   * @param {Object} data
   * @param {String} guid
   */
  sendInvoiceEmail(
    data: ISendInvoiceEmail,
    guid: string,
  ): Promise<ISendInvoiceEmailResponse> {
    return new Promise((res, rej) => {
      Methods.sendInvoiceEmail(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get the pre-reminder template for the email with link to a public version of the invoice where it can be printed or downloaded as pdf.
   * @param {String} guid
   */
  getPreReminderTemplate(guid: string): Promise<IPreReminderTemplate> {
    return new Promise((res, rej) => {
      Methods.getPreReminderTemplate(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send a pre reminder email with link to a public version of the invoice where it can be printed or downloaded as a pdf. The invoice needs to be overdue to send the reminder. A pre-reminder is a mail reminding the customer, that the invoice is overdue. This will not cause a reminder to be created in Dinero, this is only a mailout.
   * @param {Object} data
   * @param {String} guid
   */
  sendInvoicePreReminder(data: ISendPreReminderTemplate, guid: string) {
    return new Promise((res, rej) => {
      Methods.sendInvoicePreReminder(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an e-invoice to an EAN customer
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  sendInvoiceEAN(data: ISendInvoiceEAN, guid: string) {
    return new Promise((res, rej) => {
      Methods.sendInvoiceEAN(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update an existing invoice. The invoice cannot be updated if booked.
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data: ICreateInvoice, guid: string): Promise<IInvoiceUpdateResponse> {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Create a payment for an invoice. Payments can only be added to a booked invoice.
   * @param {Object} data
   * @param {String} guid
   */
  addPaymentToInvoice(
    data: IAddPaymentToInvoice,
    guid: string,
  ): Promise<IInvoiceUpdateResponse> {
    return new Promise((res, rej) => {
      Methods.addPaymentToInvoice(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete a payment from an invoice. Only booked invoices can have payments.
   * @param {Object} data
   * @param {String} guid
   */
  deletePaymentFromInvoice(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.deletePaymentFromInvoice(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get the payments for an invoice
   * @param {Object} data
   * @param {String} guid
   */
  getPaymentsForInvoice(guid: string): Promise<IPaymentsForInvoice> {
    return new Promise((res, rej) => {
      Methods.getPaymentsForInvoice(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Generate and saves a credit note draft of a given booked invoice.
   * @param {ICreateCreditNote} data
   * @param {String} guid
   */
  createCreditNoteFromInvoice(
    data: ICreateCreditNote,
    guid: string,
  ): Promise<IInvoiceUpdateResponse> {
    return new Promise((res, rej) => {
      Methods.createCreditNoteFromInvoice(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get possible templates for invoices
   */
  listInvoiceTemplates(): Promise<IInvoiceTemplate[]> {
    return new Promise((res, rej) => {
      Methods.listInvoiceTemplates(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * List mailouts
   * @param {Object} data
   * @param {String} guid
   */
  listMailouts(guid: string): Promise<IMailouts[]> {
    return new Promise((res, rej) => {
      Methods.listMailouts(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err: any) {
    console.log(err);
  }
}
