export interface IInvoice {
  Number: number;
  Guid: string;
  ExternalReference: string;
  Date: string;
  PaymentDate: string;
  Description: string;
  Status: string;
  Type: string;
  ContactName: string;
  ContactGuid: string;
  TotalInclVat: number;
  TotalExclVat: number;
  TotalInclVatInDkk: number;
  TotalExclVatInDkk: number;
  MailOutStatus: string;
  LatestMailOutType: string;
  Currency: string;
  CreatedAt: string;
  UpdatedAt: string;
  DeletedAt: string;
}

export interface IProductLinesResponse {
  ProductGuid: string;
  Description: string;
  Comments: string;
  Quantity: number;
  AccountNumber: number;
  Unit: string;
  Discount: number;
  LineType: string;
  AccountName: string;
  BaseAmountValue: number;
  BaseAmountValueInclVat: number;
  TotalAmount: number;
  TotalAmountInclVat: number;
}
export interface IProductLinesCreate {
  ProductGuid?: string;
  Description?: string;
  Comments?: string;
  Quantity: number;
  AccountNumber: number;
  Unit?: string;
  Discount: number;
  LineType?: string;
  BaseAmountValue: number;
}
export interface IInvoiceTotalResponse {
  Currency: string;
  Language: string;
  ExternalReference: string;
  Description: string;
  Comment: string;
  Date: string;
  ProductLines: IProductLinesResponse[];
  Address: string;
  Number: number;
  ContactName: string;
  ShowLinesInclVat: boolean;
  TotalExclVat: number;
  TotalVatableAmount: number;
  TotalInclVat: number;
  TotalNonVatableAmount: number;
  TotalVat: number;
  TotalLines: {
    Type: string;
    TotalAmount: number;
    Position: number;
    Label: string;
  }[];
  InvoiceTemplateId: string;
  ContactGuid: string;
  PaymentDate: string;
  PaymentConditionNumberOfDays: number;
  PaymentConditionType: string;
  CanEnableMobilePayInvoice: boolean;
}
export interface ICreateInvoice {
  Currency?: string;
  Language?: string;
  ExternalReference?: string;
  Description?: string;
  Comment?: string;
  Date?: string;
  ProductLines: IProductLinesCreate[];
  Address?: string;
  Guid?: string;
  ShowLinesInclVat?: boolean;
  InvoiceTemplateId?: string;
  ContactGuid?: string;
  PaymentConditionNumberOfDays?: number;
  PaymentConditionType?: string;
  ReminderFee?: number;
  ReminderInterestRate?: number;
  IsMobilePayInvoiceEnabled?: boolean;
}
export interface IInvoiceUpdateResponse {
  Guid: string;
  TimeStamp: string;
}
export interface IListInvoices {
  Collection: IInvoice[];
  Pagination: {
    MaxPageSizeAllowed: number;
    PageSize: number;
    Result: number;
    ResultWithoutFilter: number;
    Page: number;
  };
}
export interface IDeleteInvoice {
  Timestamp?: string;
}
export interface IBookInvoice {
  Number?: number;
  Timestamp: string;
}
export interface IInvoiceEmailTemplate {
  Sender: string;
  Receiver: string;
  Subject: string;
  Message: string;
  AddVoucherAsPdfAttachment: boolean;
}
export interface ISendInvoiceEmail {
  Timestamp?: string;
  Sender?: string;
  CcToSender?: boolean;
  Receiver?: string;
  Subject?: string;
  Message?: string;
  AddVoucherAsPdfAttachment?: boolean;
}
export interface ISendInvoiceEmailResponse {
  Recipients: {
    Email: string;
  }[];
}

export interface IPreReminderTemplate {
  Sender?: string;
  Receiver?: string;
  Subject?: string;
  Message?: string;
  AddVoucherAsPdfAttachment: boolean;
}
export interface ISendPreReminderTemplate {
  Timestamp?: string;
  Sender?: string;
  CcToSender?: boolean;
  Receiver?: string;
  Subject?: string;
  Message?: string;
  AddVoucherAsPdfAttachment?: boolean;
}
export interface ISendInvoiceEAN {
  OrderReference?: string;
  AttPerson?: string;
  Timestamp?: string;
  ReceiverEanNumber?: string;
}

export interface IAddPaymentToInvoice {
  ExternalReference?: string;
  PaymentDate?: string;
  Description: string;
  Amount: number;
  AmountInForeignCurrency?: number;
  Timestamp: string;
  DepositAccountNumber: number;
  RemainderIsFee: boolean;
}

export interface IPaymentsForInvoice {
  Payments: {
    ExternalReference: string;
    PaymentDate: string;
    Description: string;
    Amount: number;
    AmountInForeignCurrency: number;
    Guid: string;
    DepositAccountNumber: number;
  }[];
  RemainingAmount: number;
  PaidAmount: number;
  InvoiceTotalIncludingReminderExpenses: number;
  RemainderAsFeeAvailable: boolean;
}

export interface ICreateCreditNote {
  Timestamp: string;
}
export interface IInvoiceTemplate {
  Id: string;
  Name: string;
  IsDefault: boolean;
  Theme: number;
  PrimaryColor: string;
  SecondaryColor: string;
  AddressPlacement: number;
  Font: string;
  LogoType: number;
  LogoText: string;
  LogoFileGuid: string;
  MaxImageWidthMm: number;
  ShowCompanyInfo: boolean;
  ShowPaymentConditions: boolean;
  ShowLogo: boolean;
  ShowLineQuantity: boolean;
  ShowLineUnit: boolean;
  ImageFileUploadGuid: string;
  ImageTextColor: string;
}

export interface IMailouts {
  Id: number;
  Date: string;
  SeenAt: string;
  Sender: string;
  Receiver: string;
  Subject: string;
  Message: string;
  DownloadCount: number;
  PrintCount: number;
  Type: string;
  Status: string;
  LatestEvent: string;
  IsSmsMessage: boolean;
}
