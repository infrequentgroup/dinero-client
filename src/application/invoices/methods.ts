import DineroRequest from './../../libs/DineroRequest.js';

export default {
  create(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices',
    );
    return req.executePost(data);
  },
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices',
    );
    return req.executeGet();
  },
  getInvoiceTotal(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/fetch',
    );
    return req.executePost(data);
  },
  getInvoicePdf(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeFileGet();
  },
  getInvoiceJson(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  delete(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  bookInvoice(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/book',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  getInvoiceEmailTemplate(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/email/template',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  sendInvoiceEmail(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/email',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  getPreReminderTemplate(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/email/pre-reminder/template',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  sendInvoicePreReminder(
    token: string,
    orgID: string,
    data: any,
    guid: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/email/pre-reminder',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  sendInvoiceEAN(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v2/{organizationId}/invoices/{guid}/e-invoice',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  update(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1.2/{organizationId}/invoices/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePut(data);
  },
  addPaymentToInvoice(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/payments',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  deletePaymentFromInvoice(
    token: string,
    orgID: string,
    data: any,
    guid: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/payments/{paymentGuid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeDelete(data);
  },
  getPaymentsForInvoice(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v2/{organizationId}/invoices/{guid}/payments',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  createCreditNoteFromInvoice(
    token: string,
    orgID: string,
    data: any,
    guid: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/generate-creditnote',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  listInvoiceTemplates(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/templates',
    );
    return req.executeGet();
  },
  listMailouts(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{guid}/mailouts',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
};
