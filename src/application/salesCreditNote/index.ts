import Methods from './methods.js';

/**
 * SalesCreditNote manager
 * @class
 */
export default class SalesCreditNote {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Save a credit note
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  create(data: any) {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Retrieve a list of credit notes for the organization.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  list() {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update an existing credit note. The credit note cannot be updated if booked.
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete credit note. The creditnote cannot be deleted if booked.
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  delete(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get a credit note
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  get(guid: string) {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Book credit note
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  book(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.book(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Fetch a credit note to get total and line sums calculations.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  getTotals(data: any) {
    return new Promise((res, rej) => {
      Methods.getTotals(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get the email template for the email with link to a public version of the credit note where it can be printed or downloaded as pdf.
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  getEmailTemplate(guid: string) {
    return new Promise((res, rej) => {
      Methods.getEmailTemplate(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an email with link to a public version of the credit note where it can be printed or downloaded as a pdf
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  send(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.send(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get a creditnote as PDF
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  getAsPdf(guid: string) {
    return new Promise((res, rej) => {
      Methods.getAsPdf(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an e-credit note to an EAN customer
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  sendWithEAN(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.sendWithEAN(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * List mailouts
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  listMailouts(guid: string) {
    return new Promise((res, rej) => {
      Methods.listMailouts(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  processError(err: any) {
    console.log(err);
  }
}
