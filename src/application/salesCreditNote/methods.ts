import DineroRequest from './../../libs/DineroRequest.js';

export default {
  create(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes',
    );
    return req.executePost(data);
  },
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes',
    );
    return req.executeGet();
  },
  update(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1.2/{organizationId}/sales/creditnotes/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePut(data);
  },
  delete(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeDelete(data);
  },
  get(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  book(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}/book',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  getTotals(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/fetch',
    );
    return req.executePost(data);
  },
  getEmailTemplate(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}/email/template',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  send(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}/email',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  getAsPdf(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}/pdf',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  sendWithEAN(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}/e-creditNote',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  listMailouts(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/sales/creditnotes/{guid}/mailouts',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
};
