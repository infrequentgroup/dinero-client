import Methods from './methods.js';

/**
 * Reminders manager
 * @class
 */
export default class Reminders {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Get reminders
   * @param {Object} data
   * @param {String} voucherGuid
   * @returns {Promise<Object>}
   */
  getAll(voucherGuid: string) {
    return new Promise((res, rej) => {
      Methods.getAll(this.token, this.orgID, voucherGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Add reminder
   * @param {Object} data
   * @param {String} voucherGuid
   * @returns {Promise<Object>}
   */
  add(data: any, voucherGuid: string) {
    return new Promise((res, rej) => {
      Methods.add(this.token, this.orgID, data, voucherGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get reminder Define the Accept header of your request to either 'application/json' or 'application/octet-stream'.
   * @param {String} voucherGuid
   * @param {String} id
   * @returns {Promise<Object>}
   */
  get(voucherGuid: string, id: any) {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, voucherGuid, id)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update reminder
   * @param {Object} data
   * @param {String} voucherGuid
   * @param {String} id
   * @returns {Promise<Object>}
   */
  update(data: any, voucherGuid: string, id: string) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, voucherGuid, id)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete reminder
   * @param {Object} data
   * @param {String} voucherGuid
   * @param {String} id
   * @returns {Promise<Object>}
   */
  delete(data: any, voucherGuid: string, id: string) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, data, voucherGuid, id)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get next reminder
   * @param {Object} data
   * @param {String} voucherGuid
   * @returns {Promise<Object>}
   */
  getNext(voucherGuid: string) {
    return new Promise((res, rej) => {
      Methods.getNext(this.token, this.orgID, voucherGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Fetch reminder
   * @param {Object} data
   * @param {String} voucherGuid
   * @returns {Promise<Object>}
   */
  fetch(data: any, voucherGuid: string) {
    return new Promise((res, rej) => {
      Methods.fetch(this.token, this.orgID, data, voucherGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Book reminder
   * @param {Object} data
   * @param {String} voucherGuid
   * @param {String} id
   * @returns {Promise<Object>}
   */
  book(data: any, voucherGuid: string, id: string) {
    return new Promise((res, rej) => {
      Methods.book(this.token, this.orgID, data, voucherGuid, id)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get reminder email template
   * @param {Object} data
   * @param {String} voucherGuid
   * @returns {Promise<Object>}
   */
  getReminderEmailTemplate(voucherGuid: string) {
    return new Promise((res, rej) => {
      Methods.getReminderEmailTemplate(this.token, this.orgID, voucherGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an reminder email.
   * @param {Object} data
   * @param {String} voucherGuid
   * @returns {Promise<Object>}
   */
  sendReminderEmail(data: any, voucherGuid: string) {
    return new Promise((res, rej) => {
      Methods.sendReminderEmail(this.token, this.orgID, data, voucherGuid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an e-reminder to an EAN customer
   * @param {Object} data
   * @param {String} voucherGuid
   * @param {String} id
   * @returns {Promise<Object>}
   */
  sendReminderEAN(data: any, voucherGuid: string, id: string) {
    return new Promise((res, rej) => {
      Methods.sendReminderEAN(this.token, this.orgID, data, voucherGuid, id)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  processError(err: any) {
    console.log(err);
  }
}
