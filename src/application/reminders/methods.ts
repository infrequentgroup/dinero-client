import DineroRequest from './../../libs/DineroRequest.js';

export default {
  getAll(token: string, orgID: string, voucherGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    return req.executeGet();
  },
  add(token: string, orgID: string, data: any, voucherGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    return req.executePost(data);
  },
  get(token: string, orgID: string, voucherGuid: string, id: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/{id}',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    req.replaceField('{id}', id);
    return req.executeGet();
  },
  update(
    token: string,
    orgID: string,
    data: any,
    voucherGuid: string,
    id: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/{id}',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    req.replaceField('{id}', id);
    return req.executePut(data);
  },
  delete(
    token: string,
    orgID: string,
    data: any,
    voucherGuid: string,
    id: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/{id}',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    req.replaceField('{id}', id);
    return req.executeDelete(data);
  },
  getNext(token: string, orgID: string, voucherGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/next',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    return req.executeGet();
  },
  fetch(token: string, orgID: string, data: any, voucherGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/fetch',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    return req.executePost(data);
  },
  book(
    token: string,
    orgID: string,
    data: any,
    voucherGuid: string,
    id: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/{id}/book',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    req.replaceField('{id}', id);
    return req.executePost(data);
  },
  getReminderEmailTemplate(token: string, orgID: string, voucherGuid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/email/template',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    return req.executeGet();
  },
  sendReminderEmail(
    token: string,
    orgID: string,
    data: any,
    voucherGuid: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/email',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    return req.executePost(data);
  },
  sendReminderEAN(
    token: string,
    orgID: string,
    data: any,
    voucherGuid: string,
    id: string,
  ) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/invoices/{voucherGuid}/reminders/{id}/e-reminder',
    );
    req.replaceField('{voucherGuid}', voucherGuid);
    req.replaceField('{id}', id);
    return req.executePost(data);
  },
};
