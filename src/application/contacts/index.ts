import {
  IContact,
  ICreateContact,
  ICreateContactResponse,
  IListContacts,
  IUpdateContact,
} from './interface.js';
import Methods from './methods.js';

/**
 * Contacts manager
 * @class
 */
export default class Contacts {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Retrieve a list of contacts for the organization order by UpdatedAt.
   * @returns {Promise<Object>}
   */
  list(): Promise<IListContacts> {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Add a new contact to the organization
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  create(data: ICreateContact): Promise<ICreateContactResponse> {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Retrieves contact information for the contact with the given id
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  get(guid: string): Promise<IContact> {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update an existing contact
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data: IUpdateContact, guid: string) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete a contact from the given organization
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  delete(guid: string) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Restore a deleted contact from the given organization
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  restoreDeleted(guid: string) {
    return new Promise((res, rej) => {
      Methods.restoreDeleted(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err: any) {
    console.log(err);
  }
}
