import { IPagination } from './../interface';

export interface IContact {
  ExternalReference: string;
  Name: string;
  Street: string;
  ZipCode: string;
  City: string;
  CountryKey: string;
  Phone: string;
  Email: string;
  Webpage: string;
  AttPerson: string;
  VatNumber: string;
  EanNumber: string;
  PaymentConditionType: string;
  PaymentConditionNumberOfDays: number;
  IsPerson: boolean;
  IsMember: boolean;
  MemberNumber: string;
  UseCvr: boolean;
  CompanyTypeKey: ECompanyTypes;
  ContactGuid: string;
  CreatedAt: string;
  UpdatedAt: string;
  DeletedAt: string;
  IsDebitor: boolean;
  IsCreditor: boolean;
  CompanyStatus: string;
  VatRegionKey: string;
}
export interface IListContacts {
  Collection: IContact[];
  Pagination: IPagination;
}
export interface ICreateContact {
  ExternalReference?: string;
  Name: string;
  Street?: string;
  ZipCode?: string;
  City?: string;
  CountryKey: string;
  Phone?: string;
  Email?: string;
  Webpage?: string;
  AttPerson?: string;
  VatNumber?: string;
  EanNumber?: string;
  PaymentConditionType?: string;
  PaymentConditionNumberOfDays?: number;
  IsPerson: boolean;
  IsMember: boolean;
  MemberNumber?: string;
  UseCvr: boolean;
  CompanyTypeKey?: ECompanyTypes;
  ContactGuid?: string;
}
export enum ECompanyTypes {
  EmptyCompanyType,
  SoleProprietorship,
  PrivateLimitedCompany,
  PublicLimitedCompany,
  GeneralPartnership,
  LimitedPartnership,
  LimitedLiabilityCooperative,
  LimitedLiabilityVoluntaryAssociation,
  LimitedLiabilityCompany,
  EntreprenurLimitedCompany,
  Union,
  VoluntaryUnion,
  SmallPersonallyOwnedCompany,
  TrustFund,
  Others,
}
export interface ICreateContactResponse {
  ContactGuid: string;
}

export interface IUpdateContact {
  ExternalReference?: string;
  Name: string;
  Street?: string;
  ZipCode?: string;
  City?: string;
  CountryKey: string;
  Phone?: string;
  Email?: string;
  Webpage?: string;
  AttPerson?: string;
  VatNumber?: string;
  EanNumber?: string;
  PaymentConditionType?: string;
  PaymentConditionNumberOfDays?: number;
  IsPerson: boolean;
  IsMember: boolean;
  MemberNumber?: string;
  UseCvr: boolean;
  CompanyTypeKey?: ECompanyTypes;
}
