import DineroRequest from './../../libs/DineroRequest.js';

export default {
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts',
    );
    return req.executeGet();
  },
  create(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts',
    );
    return req.executePost(data);
  },
  get(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  update(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePut(data);
  },
  delete(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeDelete();
  },
  restoreDeleted(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/contacts/{guid}/restore',
    );
    req.replaceField('{guid}', guid);
    return req.executePost();
  },
};
