import Methods from './methods.js';

/**
 * Sales manager
 * @class
 */
export default class Sales {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Retrieve a list of invoices and credit notes for the organization.
   * @returns {Promise<Object>}
   */
  list() {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Returns a model containing the users default sales voucher setting preferences. These are the settings that are applied when the user creates a new invoice in Dinero's web app. The user can edit these setting under Settings>InvoiceSettings (Indstillinger>Faktura indstillinger).
   * @returns {Promise<Object>}
   */
  get() {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
