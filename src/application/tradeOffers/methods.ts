import DineroRequest from './../../libs/DineroRequest.js';

export default {
  create(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers',
    );
    return req.executePost(data);
  },
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers',
    );
    return req.executeGet();
  },
  getTotals(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/fetch',
    );
    return req.executePost(data);
  },
  getAsPdf(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  delete(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executeDelete(data);
  },
  getEmailTemplate(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/{guid}/email/template',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
  sendEmail(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/{guid}/email',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  createInvoice(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/{guid}/generate-invoice',
    );
    req.replaceField('{guid}', guid);
    return req.executePost(data);
  },
  update(token: string, orgID: string, data: any, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1.2/{organizationId}/tradeoffers/{guid}',
    );
    req.replaceField('{guid}', guid);
    return req.executePut(data);
  },
  listMailouts(token: string, orgID: string, guid: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/tradeoffers/{guid}/mailouts',
    );
    req.replaceField('{guid}', guid);
    return req.executeGet();
  },
};
