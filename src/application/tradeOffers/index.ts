import Methods from './methods.js';

/**
 * TradeOffers manager
 * @class
 */
export default class TradeOffers {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Saves a trade offer.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  create(data: any) {
    return new Promise((res, rej) => {
      Methods.create(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Retrieve a list of trade offers for the organization.
   * @returns {Promise<Object>}
   */
  list() {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Fetch a trade offer to get total and line sums.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  getTotals(data: any) {
    return new Promise((res, rej) => {
      Methods.getTotals(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get tradeoffer as json or pdf. Define the Accept header of your request to either 'application/json' or 'application/octet-stream'.
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  getAsPdf(guid: string) {
    return new Promise((res, rej) => {
      Methods.getAsPdf(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Delete trade offer
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  delete(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.delete(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get the email template for the email with to a public version of the trade offer where it can be printed or downloaded as pdf.
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  getEmailTemplate(guid: string) {
    return new Promise((res, rej) => {
      Methods.getEmailTemplate(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Send an email with link to a public version of the trade offer where it can be printed or downloaded as a pdf
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  sendEmail(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.sendEmail(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Generate a invoice of a given trade offer. (OBS Generating a invoice of the trade offer, will trigger a new timestamp on the trade offer).
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  createInvoice(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.createInvoice(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Update an existing trade offer
   * @param {Object} data
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  update(data: any, guid: string) {
    return new Promise((res, rej) => {
      Methods.createInvoice(this.token, this.orgID, data, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * List mailouts
   * @param {String} guid
   * @returns {Promise<Object>}
   */
  listMailouts(guid: string) {
    return new Promise((res, rej) => {
      Methods.listMailouts(this.token, this.orgID, guid)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err: any) {
    console.log(err);
  }
}
