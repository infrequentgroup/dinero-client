import DineroRequest from './../../libs/DineroRequest';

export default {
  list(token: string, orgID: string) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/accountingyears',
    );
    return req.executeGet();
  },
};
