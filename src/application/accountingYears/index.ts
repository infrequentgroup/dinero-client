import { IAccountingYear } from './interface.js';
import Methods from './methods.js';

/**
 * AccountingYears manager
 * @class
 */
export default class AccountingYears {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }

  list(): Promise<IAccountingYear[]> {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
