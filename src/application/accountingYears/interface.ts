export interface IAccountingYear {
  Name?: string;
  FromDate?: string;
  ToDate?: string;
}
