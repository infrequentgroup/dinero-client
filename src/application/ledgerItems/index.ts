import Methods from './methods.js';

/**
 * LedgerItems manager
 * @class
 */
export default class LedgerItems {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Get ledger items
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  get(data) {
    return new Promise((res, rej) => {
      Methods.get(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  /**
   * Update a ledger item to the ledger. It is required to send version and id for each ledger item.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  update(data) {
    return new Promise((res, rej) => {
      Methods.update(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  /**
   * Add a new ledger item to the ledger. The maximum number of ledger items in the same ledger is 1000.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  add(data) {
    return new Promise((res, rej) => {
      Methods.add(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  /**
   * Book ledger items.
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  book(data) {
    return new Promise((res, rej) => {
      Methods.book(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  /**
   * Gets status for ledger items
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  getLedgerItemStatus(data) {
    return new Promise((res, rej) => {
      Methods.getLedgerItemStatus(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }
  /**
   * Deletes a list of ledger items
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  deleteMultipleLedgers(data) {
    return new Promise((res, rej) => {
      Methods.deleteMultipleLedgers(this.token, this.orgID, data)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
