import DineroRequest from './../../libs/DineroRequest.js';

export default {
  get(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/ledgeritems/ledgers',
    );
    return req.executePost(data);
  },
  update(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/ledgeritems',
    );
    return req.executePut(data);
  },
  add(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1.2/{organizationId}/ledgeritems',
    );
    return req.executePost(data);
  },
  book(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/ledgeritems/book',
    );
    return req.executePost(data);
  },
  getLedgerItemStatus(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/ledgeritems/status',
    );
    return req.executePost(data);
  },
  deleteMultipleLedgers(token: string, orgID: string, data: any) {
    let req = new DineroRequest(
      token,
      orgID,
      'https://api.dinero.dk/v1/{organizationId}/ledgeritems/delete',
    );
    return req.executeDelete(data);
  },
};
