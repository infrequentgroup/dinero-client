import { IEntry } from './interface.js';
import Methods from './methods.js';

/**
 * Entry manager
 * @class
 */
export default class Entry {
  token: string;
  orgID: string;
  constructor(token: string, orgID: string) {
    this.token = token;
    this.orgID = orgID;
  }
  /**
   * Get a list of Entries for a given period
   * @returns {Promise<Object>}
   */
  list(): Promise<IEntry> {
    return new Promise((res, rej) => {
      Methods.list(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  /**
   * Get a list of all Entries added in a given time range, the time range cannot be longer than 31 days.
   * Primo entries will be returned if they have been updated in the time range. The value of the primo entry will be the current total for that account and accounting year, not the changes made in the time range. The guid of a primo entry will be the same for a given pair of an account and an accounting year, i.e. the guid of a primo entry on account 2000 in 2018 will never change, but the value might be updated.
   * @returns {Promise<Object>}
   */
  listChanges(): Promise<IEntry> {
    return new Promise((res, rej) => {
      Methods.listChanges(this.token, this.orgID)
        .then((response) => {
          return res(response.data);
        })
        .catch((err) => rej(this.processError(err)));
    });
  }

  processError(err) {
    console.log(err);
  }
}
