export interface IEntry {
  AccountNumber: number;
  AccountName: string;
  Date: string;
  VoucherNumber: number;
  VoucherType: string;
  Description: string;
  VatType: string;
  VatCode: string;
  Amount: number;
  EntryGuid: string;
  ContactGuid: string;
  Type: string;
}
