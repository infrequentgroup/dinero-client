import axios from 'axios';

export default class DineroFile {
  token: string;
  orgId: string;
  hosturl: string;
  constructor(token: string, orgId: string, url: string) {
    this.token = token;
    this.orgId = orgId;
    this.hosturl = this.replaceOrg(url);
  }
  replaceOrg(url: string) {
    return url.replace('{organizationId}', this.orgId);
  }
  executeUpload(data: any) {
    //NOT DONE!
    return axios.request({
      url: this.hosturl,
      method: 'POST',

      maxRedirects: 5,
      headers: this.getHeaders(),
      data: data,
    });
  }
  getHeaders() {
    return {
      Authorization: 'Bearer ' + this.token,
      'Content-Type': 'multipart/formdata',
    };
  }
}
