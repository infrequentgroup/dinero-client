import axios, { AxiosResponse } from 'axios';

export default class DineroRequest {
  token: string;
  orgId: string;
  hosturl: string;

  constructor(token: string, orgId: string, url: string) {
    this.token = token;
    this.orgId = orgId;
    this.hosturl = this.replaceOrg(url);
  }
  replaceOrg(url: string) {
    return url.replace('{organizationId}', this.orgId);
  }
  replaceField(name: string, data: string) {
    this.hosturl = this.hosturl.replace(name, data);
  }
  executeGet(): Promise<AxiosResponse<any>> {
    return axios.request({
      url: this.hosturl,
      method: 'GET',

      maxRedirects: 5,
      headers: this.getHeaders(),
    });
  }
  executeFileGet(): Promise<AxiosResponse<any>> {
    return axios.request({
      url: this.hosturl,
      method: 'GET',

      maxRedirects: 5,
      responseType: 'arraybuffer',
      headers: this.getFileHeaders(),
    });
  }
  executePost(data?: any): Promise<AxiosResponse<any>> {
    return axios.request({
      url: this.hosturl,
      method: 'POST',

      maxRedirects: 5,
      headers: this.getHeaders(),
      data: data ? data : undefined,
    });
  }
  executeDelete(data?: any): Promise<AxiosResponse<any>> {
    return axios.request({
      url: this.hosturl,
      method: 'DELETE',
      maxRedirects: 5,
      headers: this.getHeaders(),
      data: data ? data : undefined,
    });
  }
  executePut(data: any): Promise<AxiosResponse<any>> {
    return axios.request({
      url: this.hosturl,
      method: 'PUT',
      maxRedirects: 5,
      headers: this.getHeaders(),
      data: data,
    });
  }
  executePatch(data: any): Promise<AxiosResponse<any>> {
    return axios.request({
      url: this.hosturl,
      method: 'PATCH',

      maxRedirects: 5,
      headers: this.getHeaders(),
      data: data,
    });
  }
  getHeaders() {
    return {
      Authorization: 'Bearer ' + this.token,
      'Content-Type': 'application/json',
    };
  }
  getFileHeaders() {
    return {
      Authorization: 'Bearer ' + this.token,
      Accept: 'application/octet-stream',
      'Content-Type': 'application/octet-stream',
    };
  }
}
