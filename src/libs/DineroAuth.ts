import axios from 'axios';
import * as qs from 'qs';

export default class DineroAuth {
  clientId: string;
  clientSecret: string;
  orgApiKey: string;
  constructor(clientId: string, clientSecret: string, orgApiKey: string) {
    this.clientId = clientId;
    this.clientSecret = clientSecret;
    this.orgApiKey = orgApiKey;
  }
  executeAuth(): Promise<string> {
    return new Promise((res, rej) => {
      axios
        .request({
          url: 'https://authz.dinero.dk/dineroapi/oauth/token',
          method: 'POST',
          maxRedirects: 5,
          headers: this.getHeaders(),
          data: this.org(),
        })
        .then((response: { data: { access_token: string } }) => {
          return res(response.data.access_token);
        })
        .catch((response: any) => {
          rej(response);
        });
    });
  }
  org() {
    return qs.stringify({
      grant_type: 'password',
      scope: 'read write',
      username: this.orgApiKey,
      password: this.orgApiKey,
    });
  }
  token() {
    let data = this.clientId + ':' + this.clientSecret;
    return Buffer.from(data).toString('base64').toString();
  }
  getHeaders() {
    return {
      Authorization: 'Basic ' + this.token(),
      'Content-Type': 'application/x-www-form-urlencoded',
    };
  }
}
