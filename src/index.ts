import App from './application/index.js';
import DineroAuth from './libs/DineroAuth.js';

/**
 * Class to interface with Dinero Regnskab API
 * @class
 */
export default class DineroClient {
  clientSecret: string;
  clientId: string;
  constructor(clientId: string, clientSecret: string) {
    this.clientId = clientId;
    this.clientSecret = clientSecret;
  }
  /**
   * Request access token from Dinero
   * @param {String} apiKey api key from dinero account
   * @param {String} orgId id or dinero account
   * @returns {Promise<App()>} endpoints to work with.
   */
  requestToken(apiKey: string, orgId: string): Promise<App> {
    //create access token with apiKey
    return new Promise((res, rej) => {
      const auth = new DineroAuth(this.clientId, this.clientSecret, apiKey);
      auth
        .executeAuth()
        .then((token) => {
          res(new App(token, orgId));
        })
        .catch((err) => {
          rej(err);
        });
    });
  }
}
