# DineroClient

Node.js Api wrapper for Dinero.dk

Made like the [api.dinero.dk](api.dinero.dk/openapi/) documentation, use the body schemas and names of the endpoints.

## Installation

Using NPM:

```bash
npm install dinero-client
```

## Usage

### Import and initalize
```javascript
import DineroClient from  'dinero-client';

const dinero = new DineroClient("clientId","clientSecret");

//the company to access, call before every request, token expire after 1 hour
const org = await dinero.requestToken("orgApiKey","orgId");

```

### Use the requested token of the Organization

#### List all invoices
```javascript

org.invoices.list().then((invoices)=>{
 console.log(invoices);
})

```

#### Create product
```javascript

let new_product = {
  "ProductNumber": "string",
  "Name": "skrewdriver",
  "BaseAmountValue": 20,
  "Quantity": 5,
  "AccountNumber": 1000,
  "Unit": "parts",
  "ExternalReference": "Fx. WebShopID:42",
  "Comment": "Mix in a highball glass with ice."
}

org.products.create(new_product).then((create_response)=>{
 console.log(create_response);
  /*
    Response => {
     "ProductGuid": "f5a6eb71-e25c-4c30-baff-8e2cc43a9ff2"
    }
  */
})

```

## License
[MIT](https://choosealicense.com/licenses/mit/)